const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require("../auth");


//Route for creating a product
/*router.post("/", (req, res) => {
  productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
});*/

//Session 39 - Activity - Solution 1
router.post('/', auth.verify, (req, res) => {

  const userData = auth.decode(req.headers.authorization)
  
  if(userData.isAdmin) {
    productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
  } else {
    res.send({auth: "failed"})
  }
});


//Route for retrieving all the products
router.get("/all", (req, res) => {

  const userData = auth.decode(req.headers.authorization)


  productController.getAllProducts(userData).then(resultFromController => res.send(resultFromController))
});

//Route for retrieving all active products
router.get("/", (req, res) => {

  productController.getAllActive().then(resultFromController => res.send(resultFromController))

});


//Route for retrieving a specific product
router.get("/:productId", (req, res) => {

  console.log(req.params.productId)

  productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))

});


//Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {

  const isAdminData = auth.decode(req.headers.authorization)
    console.log(isAdminData)

    productController.updateProduct(req.params, req.body, isAdminData)
    .then(resultFromController => 
        res.send(resultFromController))


});



// Route to archiving a product
router.put('/archive/:productId', auth.verify, (req, res) => {

  const isAdminData = {
        productId: req.params.productId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    productController.archiveProduct(isAdminData, req.body)
    .then(resultFromController => 
        res.send(resultFromController))

});

router.put('/active/:productId', auth.verify, (req, res) => {

  const data = {
    productId : req.params.productId,
    isAdmin : auth.decode(req.headers.authorization).isAdmin
  }

  productController.activeProduct(data).then(resultFromController => res.send(resultFromController))
});






module.exports = router;