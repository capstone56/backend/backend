const express = require('express');
const router = express.Router();
const auth = require('../auth');
const orderController = require('../controllers/orderController');



//Routes


// ' / ' to addOrder
router.post('/', auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    orderController.addOrder(req.body, {userId: userData.id, isAdmin: userData.isAdmin})
    .then(resultFromController => 
        res.send(resultFromController))
});

//Retrieve user authenticated order detail
router.get('/orderdetails',auth.verify, (req,res)=>{

    const userData = auth.decode(req.headers.authorization)
    console.log(userData);

    orderController.orderDetails(req.body,userData)
    .then(resultFromController => 
        res.send(resultFromController))
});



//Retrieve user All order detail (Admin)
router.get('/allorders', auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    orderController.allOrders(userData)
    .then(resultFromController => 
        res.send(resultFromController))

});










module.exports = router