const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');


//Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {

    // The result is sent back to the frontend via the "then" method found in the route file
    return User.find({email: reqBody.email}).then(result => {

        // The "find" method returns a record if a match is found
        if (result.length > 0) {
            return true

        // No duplicate email found
        // The user is not yet registered in the database
        } else {
            return false
        }

    })

}


//User Registration
module.exports.registerUser = (reqBody) => {

    // Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
    // Uses the information from the request body to provide all the necessary information
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        // 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
        password: bcrypt.hashSync(reqBody.password, 10) 
        //hashSync(<dataToBeHash>, <saltRound>)
    })

    // Saves the created object to our database
    return newUser.save().then((user, error) => {

        // User registration failed
        if(error) {
            return false

        // User registration successful
        } else {
            return true
        }

    })
};



//User Login
module.exports.loginUser = (reqBody) => {
    return User.findOne({ email: reqBody.email }).then(result => {
        if (result == null) {
            return false
        } else {

            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if (isPasswordCorrect) {
                return { access: auth.createAccessToken(result) }
            } else {
                return false
            }


        }
    })

}


// Retrieve user details
module.exports.getProfile = (userData) => {

	return User.findById(userData.userId).then(result => {
		console.log(userData)
		if (result == null) {
			return false
		} else {
			console.log(result)

			result.password = "*****";
			return result;
		}
	});

};

