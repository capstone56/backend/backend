const Product = require('../models/Product');
const User = require('../models/User');
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');

//Create a new product
module.exports.addProduct = (reqBody) => {

  // Creates a variable "newProduct" and instantiates a new "Product" object using the mongoose model
  // Uses the information from the request body to provide all the necessary information
  let newProduct = new Product({
    name: reqBody.name,
    category: reqBody.category,
    description: reqBody.description,
    price: reqBody.price,
    quantity: reqBody.quantity
  });


  // Saves the created object to our database
  return newProduct.save().then((product, error) => {

    // Product creation failed
    if(error) {

      return false

    // Product creation successful
    } else {

      return true
    }
  })
}



//Retrieve All Product
module.exports.getAllProducts = (data) => {

  if (data.isAdmin) {
      return Product.find({}).then(result => {

      return result
    })
  } else {
    return false
  }
};



//Retrieve All Active Products
module.exports.getAllActive = () => {

  return Product.find({isActive: true, quantity: { $gt: 0 } }).then(result => {

    return result
  })
};



//Retrieve A Specific Product
module.exports.getProduct = (reqParams) => {

  return Product.findById(reqParams.productId).then(result => {

    return result
  })

};


//Update A Specific Product
module.exports.updateProduct = async (reqParams, reqBody, userData) =>{
   
    if(userData.isAdmin === true) {
        let updatedProduct = {
            name: reqBody.name,
            category: reqBody.category,
            description: reqBody.description,
            price: reqBody.price

        }
        //Syntax: findByIdAndUpdate(document ID, updatesToBeApplied)
        return await Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
        .then((updatedProduct, error)=>{
            if(error){
                return false
            } else{
                return true
            }
        })
    } else {
       return await false
    }
};



module.exports.archiveProduct = async (userData, reqBody) =>{
  if(userData.isAdmin === true) {
    let archivedProduct = {
      isActive: reqBody.isActive
    }
    //Syntax: findByIdAndUpdate(document ID, updatesToBeApplied)
    return await Product.findByIdAndUpdate(userData.productId, archivedProduct).then((archivedProduct, error)=>{
      if(error){
        return false
      } else {
        return true
      }
    })
  } else {
    return await false
  }
}


module.exports.activeProduct = (data) => {

  return Product.findById(data.productId).then((result, err) => {

    if(data.isAdmin === true) {

      result.isActive = true;

      return result.save().then((activeProduct, err) => {

        // Course not activated
        if(err) {

          return false;

        // Course activated successfully
        } else {

          return true;
        }
      })

    } else {

      //If user is not Admin
      return false
    }

  })
}



