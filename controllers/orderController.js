const Order = require('../models/Order')
const User = require('../models/User');
const auth = require('../auth');
const Product = require('../models/Product');




module.exports.addOrder =  (reqBody, userData) => {

    return User.findById(userData.userId)
            .then((result)=>{
        if(userData.isAdmin){
            return  false
        }else{

            let newOrder = new Order({
                userId: userData.userId,  
                productId: reqBody.productId,
                price: reqBody.price,
                quantity: reqBody.quantity,
                totalAmount: reqBody.quantity * reqBody.price
            });

             return  newOrder.save().then((order, error)=> {

                if(error){
                    return false
                } else {
                    return true
                }
            })
        }
    })
};


// retrieve user authenticated details
module.exports.orderDetails = (userData, reqParams) => {
    return Order.find({userId: reqParams.id})
    .then(result => {
        console.log(userData)
        if(result == null) {
            return false
        } else {
            console.log(result)
            return result
        }
    })
};


//retrieve all order details
module.exports.allOrders = (data) => {
    if(data.isAdmin){
        return Order.find({}).then(result => {
            return result
        })
    } else {
        return false
    }
};



