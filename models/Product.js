const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name: {
        type: String,
        required: [true, "Product name is required"]
    },
    category: {
        type: String,
        required: [true, "Product category is required"]
    },
    description: {
        type: String,
        required: [true, "Product description is required"]
    },
    price: {
        type: Number,
        default: 0.00
    },
    quantity: {
        type: Number,
        default: 0
    },
    isActive: {
        type: Boolean,
        default: true
    },
    created_on: {
        type: Date,
        default: new Date(),
    }

})

module.exports = mongoose.model("Product", productSchema)