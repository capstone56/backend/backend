const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "First name is required"]
	},
	email: {
		type: String,
		required: [true, "First name is required"]
	},
	password: {
		type: String,
		required: [true, "First name is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	shippingInfo: { 
        firstName: {
            type: String,
            default: ""
        },
        lastName: {
            type: String,
            default: ""
        },
        mobileNo: {
            type: String,
            default: ""
        },
        address: {
            type: String,
            default: ""
        },
        city: {
            type: String,
            default: ""
        },
        state: {
            type: String,
            default: ""
        },
        country: {
            type: String,
            default: ""
        }
    },
    orders: [
    {
        productId: {
            
        }
    }]
})

module.exports = mongoose.model("User", userSchema)