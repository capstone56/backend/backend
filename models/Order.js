const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, "User ID is required"]
    },
        productId: {
            type: String,
            required: [true, "Product ID required"],
        },
        price: {
            type: Number,
            default: 0.00,
        },
        quantity: {
            type: Number,
            default: 0,
        },
        amount:{
            type: Number,
            default: 0.00
        },
        img_path:{
            type: String,
            default: null
        },
    totalAmount:{
        type: Number,
        default: 0.00
    },
    purchased_on:{
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model("Order", orderSchema)